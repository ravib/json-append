#!/usr/bin/env python2

"""
Script to run Bills server

Usage:

    `python server.py [port]`

You can also supply the port as an environment variable
    `export BILLING_PORT=[port]`
"""

__authors__ = [
    "Ravi Bhoot (ravi2809@gmail.com)",
    "Rishav Thakker (thakker.rishav@gmail.com)",
]

import os, sys
from SimpleHTTPServer import SimpleHTTPRequestHandler
import BaseHTTPServer
import SocketServer

if len(sys.argv) > 1:
    PORT = int(sys.argv[1])
else:
    PORT = int(os.environ.get("BILLING_PORT", 80))

class CORSRequestHandler (SimpleHTTPRequestHandler):
    def end_headers (self):
        self.send_header('Access-Control-Allow-Origin', '*')
        SimpleHTTPRequestHandler.end_headers(self)

httpd = SocketServer.TCPServer(("", PORT), CORSRequestHandler)

print "serving at port", PORT
httpd.serve_forever()
